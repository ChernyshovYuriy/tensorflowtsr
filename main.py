import os
import random

import matplotlib.pyplot as plt
import numpy as np
import skimage.transform
import skimage.data
import tensorflow as tf
import time

import utils as utils

ROOT_PATH = "/home/yurii/dev"
train_data_dir = os.path.join(ROOT_PATH, "BelgiumTSC_Training")
test_data_dir = os.path.join(ROOT_PATH, "BelgiumTSC_Testing")

images, labels = utils.load_data(train_data_dir)

print("Unique Labels: {0}\nTotal Images: {1}".format(len(set(labels)), len(images)))

# utils.display_images_and_labels(images, labels)

# Let's pick label N
# utils.display_label_images(images, 0, labels)

for image in images[:5]:
    print("shape: {0}, \tmin: {1}, \tmax: {2}".format(image.shape, image.min(), image.max()))

# Resize images
images32 = [skimage.transform.resize(image, (32, 32))
            for image in images]
# display_images_and_labels(images32, labels)

for image in images32[:5]:
    print("shape: {0}, \tmin: {1}, \tmax: {2}".format(image.shape, image.min(), image.max()))

labels_a = np.array(labels)
images_a = np.array(images32)
print("labels: ", labels_a.shape, "\nimages: ", images_a.shape)

# Create a graph to hold the model.
graph = tf.Graph()

# Create model in the graph.
with graph.as_default():
    # Placeholders for inputs and labels.
    images_ph = tf.placeholder(tf.float32, [None, 32, 32, 3], name="images_ph")
    labels_ph = tf.placeholder(tf.int32, [None], name="labels_ph")

    x = tf.Variable([1.0], name="images_ph_var")
    y = tf.Variable([1], name="labels_ph_var")

    # Flatten input from: [None, height, width, channels]
    # To: [None, height * width * channels] == [None, 3072]
    images_flat = tf.contrib.layers.flatten(images_ph)

    # Fully connected layer.
    # Generates logits of size [None, 62]
    logits = tf.contrib.layers.fully_connected(images_flat, 62, tf.nn.relu)

    # Convert logits to label indexes (int).
    # Shape [None], which is a 1D vector of length == batch_size.
    predicted_labels = tf.argmax(logits, 1)

    # Define the loss function.
    # Cross-entropy is a good choice for classification.
    loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, labels_ph))

    # Create training op.
    train = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

    # And, finally, an initialization op to execute before training.
    init = tf.global_variables_initializer()

    print("images_flat: ", images_flat)
    print("logits: ", logits)
    print("loss: ", loss)
    print("predicted_labels: ", predicted_labels)

# Create a session to run the graph we created.
session = tf.Session(graph=graph)
# Create a saver.
saver = tf.train.Saver([x, y])

# First step is always to initialize all variables.
# We don't care about the return value, though. It's None.
_ = session.run([init])

for i in range(20):
    _, loss_value = session.run([train, loss],
                                feed_dict={images_ph: images_a, labels_ph: labels_a})
    if i % 10 == 0:
        print("Loss: ", loss_value)

tf.add_to_collection('train_op', images_ph)

# Pick 10 random images
# sample_indexes = random.sample(range(len(images32)), 10)
# print("Indexes:", sample_indexes)
# sample_images = [images32[i] for i in sample_indexes]
# sample_labels = [labels[i] for i in sample_indexes]

# Run the "predicted_labels" op.
# predicted = session.run([predicted_labels],
#                         feed_dict={images_ph: sample_images})[0]
# print(sample_labels)
# print(predicted)

# Display the predictions and the ground truth visually.
# fig = plt.figure(figsize=(10, 10))
# for i in range(len(sample_images)):
#     truth = sample_labels[i]
#     prediction = predicted[i]
#     plt.subplot(5, 2, 1 + i)
#     plt.axis('off')
#     color = 'green' if truth == prediction else 'red'
#     plt.text(40, 10, "Truth:        {0}\nPrediction: {1}".format(truth, prediction),
#              fontsize=12, color=color)
    # plt.imshow(sample_images[i])

# Load the test dataset.
test_images, test_labels = utils.load_data(test_data_dir)

# Transform the images, just like we did with the training set.
test_images32 = [skimage.transform.resize(image, (32, 32))
                 for image in test_images]
# utils.display_images_and_labels(test_images32, test_labels)

test_indexes = random.sample(range(len(test_images32)), 10)
print("Test Indexes:", test_indexes, " from:", len(test_labels))
test_images_selected = [test_images32[i] for i in test_indexes]
test_labels_selected = [test_labels[i] for i in test_indexes]

start = time.time()
# Run predictions against the full test set.
predicted = session.run([predicted_labels],
                        feed_dict={images_ph: [
                            skimage.transform.resize(skimage.data.imread("/home/yurii/dev/TensorFlowTSR/stop_sign.jpg"), (32, 32))
                        ]})[0]
print("Time of session run:", (time.time() - start) * 1000, " ms")

print(test_labels_selected)
print(predicted)

# Display the predictions and the ground truth visually.
# fig = plt.figure(figsize=(10, 10))
# for i in range(len(test_images_selected)):
#     truth = test_labels_selected[i]
#     prediction = predicted[i]
#     plt.subplot(5, 2, 1 + i)
#     plt.axis('off')
#     color = 'green' if truth == prediction else 'red'
#     plt.text(40, 10, "TestTruth:        {0}\nTestPrediction: {1}".format(truth, prediction),
#              fontsize=12, color=color)
    # plt.imshow(test_images_selected[i])

# plt.show()

# Calculate how many matches we got.
# match_count = sum([int(y == y_) for y, y_ in zip(test_labels_selected, predicted)])
# accuracy = match_count / len(test_labels_selected)
# print("Accuracy: {:.3f}".format(accuracy))

# Close the session. This will destroy the trained model.
session.close()

import os
import random

import matplotlib.pyplot as plt
import numpy as np
import skimage.data
import skimage.transform
import tensorflow as tf


sess = tf.Session()
new_saver = tf.train.import_meta_graph('my-model-0.meta')
new_saver.restore(sess, tf.train.latest_checkpoint('./'))

print(sess)